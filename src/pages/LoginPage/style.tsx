import React from 'react';
import styled from 'styled-components/native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';


export const Container1 = styled.View`
background-color: linear-gradient(to left bottom, rgba(254,115,115,1), rgba(255,166,116,1), rgba(255,166,116,1));
height:${hp('40%')};
align-items:center;
justify-content:center;
flex-direction:row;
gap:${wp('1%')}
`
export const Texto1 = styled.Text`
font-size:43px;
font-family: arial;
font-weight:bold;
color:white;
`
export const Texto2 = styled.Text`
font-size:26px;
font-family: arial;
color:white;
`
export const Letreiro = styled.View`
justify-content:left;
`

export const Container2 = styled.View`
background-color:white
`;
export const LoginLogo = styled.Image`
height:96.7px;
width:96.7px;
margin-top:${hp('1%')};
`
export const Form = styled.View`
margin-top:${hp('1%')};
align-items:center;
gap:${hp('3%')};
`
export const Title = styled.Text`
font-size:30px;
font-weight:medium;
padding-right:${wp('60%')}
padding-top:${hp('2%')}
`
export const Bloco = styled.View`
margin-top:${wp('10%')}
`