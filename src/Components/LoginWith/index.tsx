import React from 'react';
import { Linha, LoginWithButtons, LoginWithContainer, FacebookIcon, GoogleIcon, AppleIcon, NaoPossuiConta, LoginWithSignUpBtn, BtnText } from './style';
import {Image, Text, View} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';

export default function LoginWith(){
    return(
        <View>
        <LoginWithContainer>
            <Linha source={require('../../assets/linha.svg')} />
            <Text>ou</Text>
            <Linha source={require('../../assets/linha.svg')} />
        </LoginWithContainer>
        <LoginWithButtons>
            <TouchableOpacity>
                <FacebookIcon source={require('../../assets/Facebook.png')} />
            </TouchableOpacity>
            <TouchableOpacity>
                <GoogleIcon source={require('../../assets/Google.png')} />
            </TouchableOpacity>
            <TouchableOpacity>
                <AppleIcon source={require('../../assets/Apple.png')} />
            </TouchableOpacity>
        </LoginWithButtons>
        <NaoPossuiConta>
            <Text>Não possui conta?</Text>
            <LoginWithSignUpBtn>
               <BtnText> Cadastre-se </BtnText>
                </LoginWithSignUpBtn>
        </NaoPossuiConta>
        </View>
        
    );
}