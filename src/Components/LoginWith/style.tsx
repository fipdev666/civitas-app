import styled from 'styled-components/native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export const LoginWithContainer = styled.View`
flex-direction:row;
align-items:center;
justify-content:center;
gap:15px;
`
export const Linha = styled.Image`
height:1px;
width:145px;
`
export const LoginWithButtons = styled.View`
flex-direction:row;
justify-content:center;
align-items:center;
gap:${wp('5%')}
margin-top:${hp('5%')}
margin-bottom:${hp('5%')}
`
export const FacebookIcon = styled.Image`
width: 60px;
height: 60px;
`
export const GoogleIcon = styled.Image`
width: 60px;
height: 60px;
`
export const AppleIcon = styled.Image`
width: 60px;
height: 60px;
`
export const NaoPossuiConta = styled.View`
justify-content:center;
flex-direction:row;
`
export const LoginWithSignUpBtn = styled.TouchableOpacity`
margin-top:2px;
`
export const BtnText = styled.Text`
font-family:arial;
font-weight:bold;
color: #F04E4E;
`